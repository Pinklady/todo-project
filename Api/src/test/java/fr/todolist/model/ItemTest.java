package fr.todolist.model;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.List;

public class ItemTest {

    @Test
    public void testContentLength() {
        Assert.assertEquals(false, new Item().verifContent(""));
        Assert.assertEquals(false, new Item().verifContent("Aragorn sped on up the hill. Every now and again he bent to the ground. Hobbits go light, and their footprints are not easy even for a Ranger to read, but not far from the top a spring crossed the path, and in the wet earth he saw what he was seeking.\n" +
                "\n" +
                "'I read the signs aright,' he said to himself. 'Frodo ran to the hill-top. I wonder what he saw there? But he returned by the same way, and went down the hill again.'\n" +
                "\n" +
                "Aragorn hesitated. He desired to go to the high seat himself, hoping to see there something that would guide him in his perplexities; but time was pressing. Suddenly he leaped forward, and ran to the summit, across the great flag-stones, and up the steps. Then sitting in the high seat he looked out. But the sun seemed darkened, and the world dim and remote. He turned from the North back again to North, and saw nothing save the distant hills, unless it were that far away he could see again a great bird like an eagle high in the air, descending slowly in wide circles down towards the earth.\n" +
                "\n" +
                "Even as he gazed his quick ears caught sounds in the woodlands below, on the west side of the River. He stiffened. There were cries, and among them, to his horror, he could distinguish the harsh voices of Orcs. Then suddenly with a deep-throated call a great horn blew, and the blasts of it smote the hills and echoed in the hollows, rising in a mighty shout above the roaring of the falls.\n" +
                "\n" +
                "'The horn of Boromir!' he cried. 'He is in need!' He sprang down the steps and away, leaping down the path. 'Alas! An ill fate is on me this day, and all that I do goes amiss. Where is Sam?'\n"));

    }
}
