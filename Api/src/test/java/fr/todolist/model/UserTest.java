package fr.todolist.model;

import org.junit.Assert;
import org.junit.Test;

public class UserTest {

    @Test
    public void testInvalidEmail() {
        Assert.assertEquals(false, User.verifEmail("YouShouldNotPass@@gmail.com"));
    }

    @Test
    public void testValidEmail() {
        Assert.assertEquals(true, User.verifEmail("frantz.nestan@gmail.com"));
    }

    @Test
    public void testPasswordLength() {
        Assert.assertEquals(false, User.verifPasswordStrength("A123a*"));
        Assert.assertEquals(false, User.verifPasswordStrength("WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_WRONG_Password4ever*"));
        Assert.assertEquals(true, User.verifPasswordStrength("BEST_BEST_BEST_BEST_Password4ever*"));
    }

    @Test
    public void testPasswordCharacter() {
        Assert.assertEquals(false, User.verifPasswordStrength("A123aaaaaaaaaaaaa"));
        Assert.assertEquals(false, User.verifPasswordStrength("****aaaaa1233333333"));
        Assert.assertEquals(false, User.verifPasswordStrength("123AAAA*_____"));
        Assert.assertEquals(true, User.verifPasswordStrength("123AAAA*a_"));
    }
}
