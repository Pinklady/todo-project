package fr.todolist.service;

import fr.todolist.repository.IItemDao;
import fr.todolist.repository.ITodoListDao;
import fr.todolist.repository.IUserDao;
import fr.todolist.exception.CannotCreateItemException;
import fr.todolist.form.NewIItemForm;
import fr.todolist.model.Item;
import fr.todolist.model.TodoList;
import fr.todolist.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TodoListServiceImplTest {

    @InjectMocks
    TodoListServiceImpl todoListService;

    @Mock
    IItemDao itemDao;

    @Mock
    IUserDao userDao;

    @Mock
    ITodoListDao todoListDao;

    @Mock
    User user;

    @Mock
    TodoList todo;

    @Mock
    Item item;

    @Mock
    List<Item> items;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = CannotCreateItemException.class)
    public void createTodoListTestWhenUserIsNotValid() throws CannotCreateItemException {
        TodoList todoList = new TodoList();
        todoList.setUser(user);
        todoList.setId(1L);
        when(userDao.findById(1L)).thenReturn(Optional.of(user));
        when(user.isValid()).thenReturn(false);
        TodoList todoList1 = todoListService.createTodoList(1L);
    }

    @Test(expected = CannotCreateItemException.class)
    public void createItemTestWhenUserIsNotValid() throws CannotCreateItemException {
        when(user.isValid()).thenReturn(false);
        when(items.size()).thenReturn(1);
        when(todo.getItems()).thenReturn(items);
        when(todo.getItems().get(0)).thenReturn(item);
        when(todo.getItems().get(0).getCreationDate()).thenReturn(LocalDateTime.of(2010, 9, 10, 10, 30));
        TodoList todoList = new TodoList();
        System.out.println(items.get(0).getCreationDate());
        todoList.setItems(items);
        todoList.setUser(user);
        System.out.println(todoList.getItems().size());
        todoList.addItem("you should not pass", "I am looking for someone to share in an adventure that I am arranging, and it's very difficult to find anyone.'\n" +
                "I should think so — in these parts! We are plain quiet folk and have no use for adventures. Nasty disturbing uncomfortable things! Make you late for dinner!");
    }

    @Test(expected = CannotCreateItemException.class)
    public void createItemTestWhenTosoonToCreate() throws CannotCreateItemException {
        when(items.size()).thenReturn(1);
        when(todo.getItems()).thenReturn(items);
        when(todo.getItems().get(0)).thenReturn(item);
        when(todo.getItems().get(0).getCreationDate()).thenReturn(LocalDateTime.now());
        TodoList todoList = new TodoList();
        todoList.setItems(items);
        todoList.setUser(user);
        System.out.println(todoList.getItems().size());
        todoList.addItem("you should not pass", "May the hair on your toes never fall out!");
    }

    @Test(expected = CannotCreateItemException.class)
    public void createItemTestWhenNameUnavailable() throws CannotCreateItemException {
        when(user.isValid()).thenReturn(true);
        when(items.size()).thenReturn(1);
        when(todo.getItems()).thenReturn(items);
        when(todo.getItems().get(0)).thenReturn(item);
        when(todo.getItems().get(0).getName()).thenReturn("");
        when(todo.getItems().get(0).getCreationDate()).thenReturn(LocalDateTime.of(2010, 9, 10, 10, 30));
        TodoList todoList = new TodoList();
        System.out.println(items.get(0).getName());
        todoList.setItems(items);
        todoList.setUser(user);
        System.out.println(todoList.getItems().size());
        todoList.addItem(items.get(0).getName(), "May the hair on your toes never fall out!");
    }

    @Test(expected = CannotCreateItemException.class)
    public void createItemTestWhenListIsFull() throws CannotCreateItemException {
        when(items.size()).thenReturn(10);
        when(todo.getItems()).thenReturn(items);
        when(todo.getItems().get(9)).thenReturn(item);
        when(todo.getItems().get(9).getCreationDate()).thenReturn(LocalDateTime.of(2010, 9, 10, 10, 30));
        TodoList todoList = new TodoList();
        todoList.setItems(items);
        todoList.setUser(user);
        System.out.println(todoList.getItems().size());
        todoList.addItem("you should not pass", "There is nothing like looking, if you want to find something.");
    }
}
