package fr.todolist.service;

import fr.todolist.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceTest {

    @Mock
    User user;

    @InjectMocks
    EmailServiceImpl emailService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void sendEmailTestIsPossible(){
        Mockito.when(user.getBirthdate()).thenReturn(LocalDate.of(1996,9,19));
        Assert.assertTrue(emailService.sendEmail(user));
    }

    @Test
    public void sendEmailTestIsImpossible(){
        Mockito.when(user.getBirthdate()).thenReturn(LocalDate.of(2007,9,19));
        Assert.assertFalse(emailService.sendEmail(user));
    }
}
