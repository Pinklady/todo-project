package fr.todolist.service;

import fr.todolist.repository.IUserDao;
import fr.todolist.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceimplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    IUserDao userDao;


    @Test
    public void createUserTest() {
        User user = new User();
        user.setId(1L);
        user.setEmail("frantz.nestan@gmail.com");
        user.setFirstname("frantz");
        user.setLastname("nestan");
        user.setPassword("Password123**");
        user.setBirthdate(LocalDate.of(1996, 9, 19));
        when(userDao.save(user)).thenReturn(user);
        User user1 = userService.createUser(user);
        Assert.assertEquals(user, user1);

    }
}
