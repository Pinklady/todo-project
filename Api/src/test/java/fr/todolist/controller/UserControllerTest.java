package fr.todolist.controller;

import fr.todolist.TodoListApplication;
import fr.todolist.model.User;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootTest(classes = TodoListApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerTest {

    @LocalServerPort
    private int port = 8080;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    @Ignore
    public void testAddUser() {
        User user = new User();
        user.setBirthdate(LocalDate.of(1996,9,19));
        user.setEmail("nestanfrantz@gmail.com");
        user.setLastname("nestan");
        user.setFirstname("frantz");
        user.setPassword("password");
        ResponseEntity<User> responseEntity = this.restTemplate.postForEntity("http://localhost:" + port + "/user/create", user, User.class);
        Assert.assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    @Ignore
    public void testgetUser() {
        ResponseEntity<User[]> responseEntity = this.restTemplate.getForEntity("http://localhost:" + port + "/user/all",User[].class);
        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }
}
