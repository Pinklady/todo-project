package fr.todolist.form;

public class NewIItemForm {
    public Long todoListId;
    public String content;
    public String name;

    @Override
    public String toString() {
        return "NewIItemForm{" +
                "todoListId=" + todoListId +
                ", content='" + content + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
