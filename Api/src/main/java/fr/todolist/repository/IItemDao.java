package fr.todolist.repository;

import fr.todolist.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IItemDao extends JpaRepository<Item,Long> {
}
