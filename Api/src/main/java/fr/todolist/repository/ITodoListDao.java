package fr.todolist.repository;

import fr.todolist.model.TodoList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITodoListDao extends JpaRepository<TodoList, Long> {
}
