package fr.todolist.controller;

import fr.todolist.exception.CannotCreateItemException;
import fr.todolist.form.NewIItemForm;
import fr.todolist.model.Item;
import fr.todolist.model.TodoList;
import fr.todolist.service.TodoListServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RequestMapping("/todo")
public class TodoListController {
    @Autowired
    private TodoListServiceImpl todoListService;

    @GetMapping(path = "/create")
    ResponseEntity<TodoList> createTodoList(@RequestHeader("userId") String userId) throws CannotCreateItemException {
        return new ResponseEntity<TodoList>(todoListService.createTodoList(Long.parseLong(userId)), HttpStatus.CREATED);
    }

    @GetMapping(path = "/todolist/{id}")
    ResponseEntity<TodoList> getTodoListById(@PathVariable String id) throws CannotCreateItemException {
        return new ResponseEntity<TodoList>(todoListService.getTodoList(Long.parseLong(id)), HttpStatus.OK);
    }

    @PostMapping(path = "/createItem")
    ResponseEntity<Item> createItem(@RequestBody NewIItemForm newIItemForm) {
        return new ResponseEntity<Item>(todoListService.addItem(newIItemForm), HttpStatus.CREATED);
    }

    @DeleteMapping(path = "/deleteItem/{id}/{todo}")
    ResponseEntity<String> deleteItem(@PathVariable String id, @PathVariable String todo) {
        return new ResponseEntity<String>(todoListService.deleteItem(id, todo), HttpStatus.CREATED);
    }
}
