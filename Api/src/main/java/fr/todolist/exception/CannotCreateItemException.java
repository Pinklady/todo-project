package fr.todolist.exception;

public class CannotCreateItemException  extends Exception{

    public CannotCreateItemException(String message) {
        super(message);
    }
}
