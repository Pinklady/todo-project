package fr.todolist.service;

import fr.todolist.repository.IItemDao;
import fr.todolist.repository.ITodoListDao;
import fr.todolist.repository.IUserDao;
import fr.todolist.exception.CannotCreateItemException;
import fr.todolist.form.NewIItemForm;
import fr.todolist.model.Item;
import fr.todolist.model.TodoList;
import fr.todolist.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TodoListServiceImpl implements ITodoListService {
    @Autowired
    IUserDao userDao;

    @Autowired
    ITodoListDao todoListDao;

    @Autowired
    IItemDao itemDao;


    @Autowired
    IEmailService EmailService;

    @Override
    public TodoList createTodoList(Long userId) throws CannotCreateItemException {
        Optional<User> user = userDao.findById(userId);
        if (user.get().isValid()) {
            TodoList todoList = new TodoList();
            todoList.setUser(user.get());
            TodoList todoList1 = todoListDao.save(todoList);
            return todoList1;
        }
        throw new CannotCreateItemException("Vous ne pouvez pas crée de todo list");
    }

    @Override
    public Item addItem(NewIItemForm newIItemForm) {
        try {
            Optional<TodoList> todoList = todoListDao.findById(newIItemForm.todoListId);
            Item item1 = todoList.get().addItem(newIItemForm.name, newIItemForm.content);
            this.EmailService.sendEmail(todoList.get().getUser());
            return itemDao.save(item1);
        } catch (CannotCreateItemException cannotCreateItemException) {
            System.out.println(cannotCreateItemException.getMessage());
        }
        return null;
    }

    @Override
    public TodoList getTodoList(long parseLong) {
        Optional<TodoList> todoList = todoListDao.findById(parseLong);
        return todoList.get();
    }

    @Override
    public String deleteItem(String id , String todoId) {
        Item item = itemDao.getOne(Long.parseLong(id));
        TodoList todoList = todoListDao.getOne(Long.parseLong(todoId));
        todoList.getItems().remove(item);
        itemDao.delete(item);
        return item.getId().toString();
    }


}
