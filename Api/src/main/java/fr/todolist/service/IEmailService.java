package fr.todolist.service;

import fr.todolist.model.User;

public interface IEmailService {
    public boolean sendEmail(User user);
}
