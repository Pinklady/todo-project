package fr.todolist.service;

import fr.todolist.exception.CannotCreateItemException;
import fr.todolist.form.NewIItemForm;
import fr.todolist.model.Item;
import fr.todolist.model.TodoList;

public interface ITodoListService {
    TodoList createTodoList(Long userId) throws CannotCreateItemException;

    Item addItem(NewIItemForm newIItemForm);

    TodoList getTodoList(long parseLong);

    String deleteItem(String id, String todoId);
}
