package fr.todolist.service;

import fr.todolist.repository.IUserDao;
import fr.todolist.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements IUserService{
    @Autowired
    IUserDao userDao;
    @Override
    public User createUser(User user) {
        User user1 = userDao.save(user);
        return user1;
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.findAll();
    }
}