package fr.todolist.service;

import fr.todolist.model.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IUserService {
    public User createUser(User user);
    public List<User> getAllUsers();
}
//String email, String password, String lastName, String firstName, LocalDate birthDate