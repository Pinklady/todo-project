package fr.todolist.service;

import fr.todolist.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class EmailServiceImpl implements IEmailService{
    @Override
    public boolean sendEmail(User user) {
        return LocalDate.now().minusYears(18).isAfter(user.getBirthdate());
    }
}
