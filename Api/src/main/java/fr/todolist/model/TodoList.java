package fr.todolist.model;

import fr.todolist.exception.CannotCreateItemException;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.time.temporal.ChronoUnit.SECONDS;

@Entity
@Table(name = "todo")
public class TodoList {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @OneToMany()

    private List<Item> items = new ArrayList<>();
    @OneToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    public Item addItem(String name, String content) throws CannotCreateItemException {
        if (this.items.size() > 0) {
            if (this.items.stream().filter(item -> item.getName().equals(name)).count() >= 1) {
                throw new CannotCreateItemException("Un item porte déja ce nom");
            }
        }
        if (this.getItems().size() > 0) {
            Item item = getItems().get(getItems().size() - 1);
            Duration time = Duration.between(LocalDateTime.now(), item.getCreationDate());
            if (Math.abs(time.get(SECONDS)) < 2) {
                throw new CannotCreateItemException("Vous devez patienter " + time.get(SECONDS) +
                        " avant de créer un nouvel item");
            } else if (getItems().size() >= 10) {
                throw new CannotCreateItemException("Vous avez trop d'items pour cette todo list");
            } else if (!this.getUser().isValid()) {
                throw new CannotCreateItemException("Vous devez être valid pour creer un item");
            } else if (!item.verifName(name)) {
                throw new CannotCreateItemException("Vous devez être valid pour creer un item");
            } else {
                Item item1 = new Item(name, content, LocalDateTime.now());
                this.getItems().add(item1);
                return item1;
            }
        } else {
            Item item1 = new Item(name, content, LocalDateTime.now());
            this.items.add(item1);
            return item1;
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Item> getItems() {
        return items;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
