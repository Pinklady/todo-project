package fr.todolist.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "items")
public class Item {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "content")
    private String content;
    @Column(name = "creationDate")
    private LocalDateTime creationDate;

    public Item() {

    }


    public Item(String name, String content) {
        if(verifContent(content) && verifName(name)){
            this.name = name;
            this.content = content;
        }
    }

    public Item(String name, String content, LocalDateTime creationDate) {
        if(verifContent(content) && verifName(name)){
            this.name = name;
            this.content = content;
            this.creationDate = creationDate;
        }
    }

    public boolean verifContent(String content){
        if(content.length()==0 || content.length()>1000) return false;
        return true;
    }

    public boolean verifName(String name){
        if(name.length() == 0) return false;
        return true;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }
}
