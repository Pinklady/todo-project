package fr.todolist.model;

import org.junit.platform.commons.util.StringUtils;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "firstname")
    private String firstname;
    @Column(name = "birthdate")
    private LocalDate birthdate;

//
//    public User(String email, String password, String lastName, String firstName, LocalDate birthDate) {
//        this.email = email;
//        this.password = password;
//        this.lastName = lastName;
//        this.firstName = firstName;
//        this.birthDate = birthDate;
//    }

    public boolean isValid() {
        if (!this.verifEmail(this.email) || !this.verifPasswordStrength(this.password)) return false;
        return StringUtils.isNotBlank(this.firstname)
                && StringUtils.isNotBlank(this.lastname)
                && birthdate != null
                && LocalDate.now().minusYears(13).isAfter(this.birthdate);
    }

    public static boolean verifEmail(String email) {
        String regex = "^[-a-z0-9~!$%^&*_=+}{\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$";
        if (!email.matches(regex)) {
            return false;
        }
        return true;
    }

    public static boolean verifPasswordStrength(String password) {
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\\-_/]).{8,40}$";
        if (!password.matches(regex)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastName) {
        this.lastname = lastName;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthDate) {
        this.birthdate = birthDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", birthdate=" + birthdate +
                '}';
    }
}
