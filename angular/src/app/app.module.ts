import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {UserComponent} from './user/user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormlyModule} from '@ngx-formly/core';
import {FormlyFieldInput} from './formly-field-input';
import {FormlyFieldDate} from './formly-field-date';
import {TodoListComponent} from './todo-list/todo-list.component';
import {TodoDetailComponent} from './todo-detail/todo-detail.component';


@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    FormlyFieldInput,
    FormlyFieldDate,
    TodoListComponent,
    TodoDetailComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([{path: 'user', component: UserComponent},
      {path: 'todolist', component: TodoListComponent},
      {path: 'todolist/:id', component: TodoDetailComponent}
    ]),
    FormsModule,
    ReactiveFormsModule,
    FormlyModule.forRoot({
      types: [
        {name: 'input', component: FormlyFieldInput},
        {name: 'date', component: FormlyFieldDate},
      ],
      validationMessages: [
        {name: 'required', message: 'This field is required'},
      ]
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
