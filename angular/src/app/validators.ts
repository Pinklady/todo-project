import {FormControl, ValidationErrors} from '@angular/forms';

export function emailValidator(control: FormControl): ValidationErrors {
  return /\S+@\S+\.\S+/.test(control.value) ? null : { email: true };
}

export function passwordValidator(control: FormControl): ValidationErrors {
  return /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*\-_/]).{8,40}$/.test(control.value) ? null : { password: true };
}
