import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TodolistService} from '../../services/todolist.service';
import {User} from '../../models/user';
import {Todolist} from '../../models/todolist';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  todolist: Todolist;

  constructor(private todolistService: TodolistService, private router: Router) {
  }

  ngOnInit(): void {
  }

  create() {
    const user: User = JSON.parse(localStorage.getItem('user'));
    this.todolistService.createTodoList(user.id).subscribe((response) => {
      this.todolist = response;
      localStorage.setItem('todo', JSON.stringify(this.todolist));
      this.router.navigate(['todolist', this.todolist.id]);
    });
  }
}
