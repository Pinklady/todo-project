import {Component, OnInit} from '@angular/core';
import {AbstractControl, EmailValidator, FormControl, FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {Router} from '@angular/router';
import differenceInYears from 'date-fns/differenceInYears';
import {parseISO} from 'date-fns';
import {emailValidator, passwordValidator} from '../validators';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'email',
      type: 'input',
      id: 'email',
      templateOptions: {
        label: 'Email address',
        placeholder: 'Enter email',
        required: true,
      },
      validators: {
        validation: [emailValidator],
      }
    },
    {
      key: 'lastname',
      type: 'input',
      id: 'lastname',
      templateOptions: {
        label: 'Lastname',
        placeholder: 'Enter lastname',
        required: true,
      }
    },
    {
      key: 'firstname',
      type: 'input',
      id: 'firstname',
      templateOptions: {
        label: 'Firstname',
        placeholder: 'Enter firstname',
        required: true,
      }
    },
    {
      key: 'password',
      type: 'input',
      id: 'password',
      templateOptions: {
        label: 'Password',
        placeholder: 'Enter password',
        required: true,
      },
      validators: {
        validation: [passwordValidator],
      }
    },
    {
      key: 'birthdate',
      type: 'date',
      id: 'birthdate',
      templateOptions: {
        label: 'Birthdate',
        placeholder: 'Enter birthdate',
        required: true,
      }
    }
  ];

  errors = [];

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit(model: any) {
    this.errors = [];
    Object.keys(this.form.controls).forEach(c => {

      if (this.form.get(c).invalid) {
        if (!this.form.get(c).errors.required && this.form.get(c).errors.email) {
          this.errors.push(`email incorrect`);
        } else if (!this.form.get(c).errors.required && this.form.get(c).errors.password) {
          this.errors.push(`Veuillez entrer un mot de passe plus sécurisé`);
        } else {
          this.errors.push(`${c} est requis`);
        }

      }
    });
    if (differenceInYears(new Date(), parseISO(model.birthdate)) < 13) {
      this.errors.push('Vous êtes trop jeune pour vous inscrire');
    }
    if (this.errors.length === 0) {
      this.userService.createUser(model).subscribe((response) => {
        this.user = response;
        localStorage.setItem('user', JSON.stringify(this.user));
        this.form.reset();
        this.router.navigate(['todolist']);
      });
    }
  }
}
