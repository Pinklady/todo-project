import {Component, OnInit} from '@angular/core';
import {TodolistService} from '../../services/todolist.service';
import {Todolist} from '../../models/todolist';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {

  todolist: Todolist = {} as any;

  form = new FormGroup({});
  model = {};
  fields: FormlyFieldConfig[] = [
    {
      key: 'name',
      type: 'input',
      id: 'name',
      templateOptions: {
        label: 'Item name',
        placeholder: 'Enter name',
        required: true,
      }
    },
    {
      key: 'content',
      type: 'input',
      id: 'content',
      templateOptions: {
        label: 'Content',
        placeholder: 'Enter content',
        required: true,
      }
    },
  ];
  errors = [];

  constructor(private todolistService: TodolistService, private router: Router) {
  }

  ngOnInit(): void {
    this.getTodolist();
  }


  delItem(id: number) {
    this.todolistService.removeItem(id, this.todolist.id).subscribe((response) => {
      console.log(response)
      this.todolist.items = this.todolist.items.filter(item => item.id !== +response);
      //this.router.navigate(['todolist/' , this.todolist.id]);
    });
  }

  onSubmit(model: any) {
    Object.keys(this.form.controls).forEach(c => {
      if (this.form.get(c).invalid) {
        this.errors.push(`${c} est requis`);
      }
    });
    if (this.errors.length === 0) {
      const data = {name: model.name, content: model.content, todoListId: this.todolist.id};
      this.todolistService.createItem(data).subscribe((response) => {
        //this.router.navigate(['todolist/' + this.todolist.id]);
        this.todolist.items.push(response);
        this.form.reset();
      });
    }
  }

  getTodolist() {
    const todo = JSON.parse(localStorage.getItem('todo'));
    this.todolistService.getTodo(todo.id).subscribe(response => this.todolist = response);
  }
}
