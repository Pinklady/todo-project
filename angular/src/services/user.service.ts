import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
  }

  createUser(data: any): Observable<User> {
    console.log(data);
    return this.httpClient.post<User>('http://localhost:8080/user/create', data);
  }
}
