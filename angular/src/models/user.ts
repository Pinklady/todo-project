export interface User {

  id: number;
  email: string;
  password: string;
  lastname: string;
  firstname: string;
  birthdate: Date;
}


