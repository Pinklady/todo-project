import {Todolist} from './todolist';

export interface Item {
  id: number;
  name: string;
  content: string;
  creationDate: Date;
  todoList: Todolist;
}
