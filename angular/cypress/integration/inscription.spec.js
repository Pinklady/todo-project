describe('Inscription', () => {
  it("Soumission du formulaire avec les champs remplis", () => {
    cy.visit('http://localhost:4200/user');
    cy.get("#email").type('sasuke@gmail.com');
    cy.get("#lastname").type('lastname');
    cy.get("#firstname").type('firstname');
    cy.get("#password").type('Frantz1996**');
    cy.get("#birthdate").type('1996-09-19');
    cy.get("#valider").click();
    cy.get("#errors").should('not.be.visible')
  });
  it("Soumission du formulaire avec un champ manquant", () => {
    cy.visit('http://localhost:4200/user');
    cy.get("#email").type('sasuke@gmail.com');
    cy.get("#lastname").type('lastname');
    cy.get("#firstname").type('firstname');
    cy.get("#password").type('Frantz1996**');
    cy.get("#birthdate").clear();
    cy.get("#valider").click();
    cy.get("#errors").should('contain', 'birthdate est requis');
  })
  it("l'utilisatuer est trop jeune", () => {
    cy.visit('http://localhost:4200/user');
    cy.get("#email").type('sasuke@gmail.com');
    cy.get("#lastname").type('lastname');
    cy.get("#firstname").type('firstname');
    cy.get("#password").type('Frantz1996**');
    cy.get("#birthdate").type('2010-09-19');
    cy.get("#valider").click();
    cy.get("#errors").should('contain', 'Vous êtes trop jeune pour vous inscrire');
  })
  it("l'email incorrect", () => {
    cy.visit('http://localhost:4200/user');
    cy.get("#email").type('123');
    cy.get("#lastname").type('lastname');
    cy.get("#firstname").type('firstname');
    cy.get("#password").type('Frantz1996**');
    cy.get("#birthdate").type('2010-09-19');
    cy.get("#valider").click();
    cy.get("#errors").should('contain', 'email incorrect');
  })
  it("mot de pas pas sécurisé", () => {
    cy.visit('http://localhost:4200/user');
    cy.get("#email").type('123');
    cy.get("#lastname").type('lastname');
    cy.get("#firstname").type('firstname');
    cy.get("#password").type('password');
    cy.get("#birthdate").type('2010-09-19');
    cy.get("#valider").click();
    cy.get("#errors").should('contain', 'Veuillez entrer un mot de passe plus sécurisé');
  })
  it("tester l'appel de la route", () => {
    cy
      .request('POST', 'http://localhost:8080/user/create', {
        email: "email@gmail.com", lastname: "lastname",
        firstname: "firstname", password: "Frantz2019**", birthdate: "1996-09-19"
      })
      .then((response) => {
        // response.body is automatically serialized into JSON
        expect(response.body).to.have.property("email", "email@gmail.com") // true
        expect(response.body).to.have.property("lastname", "lastname",) // true
        expect(response.body).to.have.property("firstname", "firstname") // true
        expect(response.body).to.have.property("password", "Frantz2019**") // true
        expect(response.body).to.have.property("birthdate", "1996-09-19") // true
      })
  })
  it("tester l'appel de la routese passe mal", () => {
    cy
      .request('POST', 'http://localhost:8080/user/create', {
        email: "email@gmail.com", lastname: "lastname",
        firstname: "firstname", password: "Frantz2019**", birthdate: "1996-09-19"
      })
      .then((response) => {
        // response.body is automatically serialized into JSON
        expect(response.body).to.have.property("email", "email@gmail.com") // true
        expect(response.body).to.not.have.property("lastname", "uhuh") // true
        expect(response.body).to.have.property("firstname", "firstname") // true
        expect(response.body).to.have.property("password", "Frantz2019**") // true
        expect(response.body).to.have.property("birthdate", "1996-09-19") // true
      })
  })
})

//Veuillez entrer un mot de passe plus sécurisé
